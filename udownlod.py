from tkinter import *
from pytube import YouTube
from tkinter import ttk
from tkinter.filedialog import askdirectory
import ffmpeg
import os
from pathlib import *


class Youtube:
    error_count = 0

    def __init__(self, master):
        self.master = master
        master.title('Youtube_Downloder')
        master.resizable(False, False)
        self.screen = Entry(master, state='normal', width=25, background="White", foreground="blue",
                            font=('Arial', 16))
        self.screen.grid(row=1, column=0, columnspan=4, padx=5, pady=5)
        self.layout = Label(master, text='Past current Link below up to 5', font=('Arial', 16), foreground="green")
        self.layout.grid(row=0, column=1)
        self.create_button('Add Link', 1, 30)
        self.create_button('Save In', 2, 30)
        self.create_button('Download', 3, 30)

        self.links = []
        self.new_link = []
        self.count = 0
        self.row = 3
        self.save_in = 'D:/'
        self.radsel = None
        self.resolution = {}
        self.f0 = None
        self.f2 = None
        self.f3 = None
        self.f4 = None
        self.f5 = None
        self.video = None
        self.audio = None
        self.title = None

    def frame_one(self):
        f_1 = self.layout_frame('Link_01')
        f_1.grid(row=5,column=1)
        return f_1

    def frame_two(self):
        f_2 = self.layout_frame('Link_02')
        f_2.grid(row=7, column=1)
        return f_2

    def frame_three(self):
        f_3 = self.layout_frame('Link_03')
        f_3.grid(row=9, column=1)
        return f_3

    def frame_four(self):
        f_4 = self.layout_frame('Link_04')
        f_4.grid(row=11, column=1)
        return f_4

    def frame_five(self):
        f_5 = self.layout_frame('Link_05')
        f_5.grid(row=13, column=1)
        return f_5

    def layout_frame(self,text):
        return LabelFrame(self.master,text=text,width=20,height=5,font=('Arial', 16), foreground="Blue")

    def create_button(self, val, row, column, width=12):
        button = Button(self.master, text=val, width=width, command=lambda: self.click(val))
        return button.grid(row=row, column=column)

    def click(self, clicked_button):
        if clicked_button == 'Add Link' and self.count < 5 and self.screen.get() is not '' and \
                self.screen.get() is not 'same link try again':
            self.download_links()
            self.count += 1
            print(self.count)

        elif clicked_button == 'Download':
            self.screen.configure(state='disable')
            self.co = 0
            down = [False,False,False,False,False]
            self.frame_layer = [self.f0,self.f2,self.f3,self.f4,self.f5]
            print(self.new_link)
            frame = BaseRadioButton.my_resolution
            for url,resolution in frame.items():
                if len(frame.values()) == len(self.new_link):

                    down = ProgressBarFrame(self.frame_layer[self.co])
                    self.video,self.audio,self.title = down.progress_to_my_url(url)
                    E = self.title.replace("|",'_')
                    E = E.replace(":",'_')
                    E = E.replace(".", '_')
                    E = E.replace("'", '_')
                    E = E.replace('"', '_')
                    d = (str(self.save_in) + '/' + E + '.mp4')
                    print(d)


                    if self.down_it_load(resolution) == True:

                        self.create_label(self.frame_layer[self.co], text='start_Merging...', font=('Arial', 8),
                                          row=3, column=0, foreground="red")
                        video_stream = ffmpeg.input(str(self.save_in)+'/'+str(self.co)+'video.mp4')
                        print(video_stream)
                        audio_stream = ffmpeg.input(str(self.save_in)+'/'+str(self.co)+'audio.mp4')
                        print(audio_stream)
                        ffmpeg.output(audio_stream, video_stream,d).run()
                        self.create_label(self.frame_layer[self.co], text='Merge is completed', font=('Arial', 8),
                                          row=3, column=0, foreground="red")
                        self.co += 1

        elif clicked_button == 'Save In':
            dir_name = askdirectory(initialdir='D:/', parent=root)
            self.save_in = dir_name

    def down_it_load(self,resolution):
        res = ["144p", "360p", "720p", "1080p"]
        d_video = self.video.get(resolution)
        if d_video is not 0:
            try:
                d_video.download(self.save_in,str(self.co)+'video')
                self.audio.download(self.save_in,str(self.co)+'audio')
                return True

            except:
                self.create_label(self.frame_layer[self.co], text='url not working for this res', font=('Arial', 8),
                                  row=3, column=0, foreground="red")

        else:
            for i in res:
                d_video = self.video.get(i)
                if d_video is not 0:
                    d_video.download(self.save_in, str(self.co)+'video')
                    self.audio.download(self.save_in, str(self.co)+'audio')
                    self.create_label(self.frame_layer[self.co], text='url download with ' + str(i),
                                      font=('Arial', 8),
                                      row=3, column=0, foreground="red")
                    return True
                    break


    def create_label(self,label, text, row=None, column=None, font=('Arial', 10),foreground=None):
        return Label(label, text=text, font=font, width=25,anchor=W, justify=LEFT,foreground=foreground).grid(row=row, column=column)

    def download_links(self):
        if self.screen.get() not in self.new_link:
            self.links.append(self.screen.get())
            print(self.links)
            self.screen.delete(0, END)

            if self.links[0] is not None and self.links[0] not in self.new_link:
                self.f0 = self.frame_one()
                self.create_label(self.f0,str(1)+'. ' + self.links[0], 1, 0)
                self.new_link.append(self.links[0])
                FrameOneBaseRadioButton(self.f0)
            elif self.links[1] is not None and self.links[1] not in self.new_link:
                self.f2 = self.frame_two()
                self.create_label(self.f2, str(1) + '. ' + self.links[1], 1, 0)
                self.new_link.append(self.links[1])
                FrameTwoBaseRadioButton(self.f2)
            elif self.links[2] is not None and self.links[2] not in self.new_link:
                self.f3 = self.frame_three()
                self.create_label(self.f3, str(1) + '. ' + self.links[2], 1, 0)
                self.new_link.append(self.links[2])
                FrameThreeBaseRadioButton(self.f3)
            elif self.links[3] is not None and self.links[3] not in self.new_link:
                self.f4 = self.frame_four()
                self.create_label(self.f4, str(1) + '. ' + self.links[3], 1, 0)
                self.new_link.append(self.links[3])
                FrameFourBaseRadioButton(self.f4)
            elif self.links[4] is not None and self.links[4] not in self.new_link:
                self.f5 = self.frame_five()
                self.create_label(self.f5, str(1) + '. ' + self.links[4], 1, 0)
                self.new_link.append(self.links[4])
                FrameFiveBaseRadioButton(self.f5)
            print('new link: ' + str(self.new_link))
        else:
            self.count -= 1
            self.screen.delete(0, END)
            self.screen.insert(0,"same link try again")


class Download:                        #https://youtu.be/put3FVr4n0w?t=15
    def __init__(self,progress):
        self.resolution = []
        self.progress = progress

    def get_stream(self,i):
        data_dic = {}
        my = YouTube(i,on_progress_callback=self.progress_function)
        res = ["144p","360p","720p","1080p"]
        audio = my.streams.filter(file_extension='mp4',only_audio=True).first()
        for r in range(0,4):
            stream = my.streams.filter(file_extension='mp4',adaptive=True,resolution=res[r]).first()
            if stream is not None:
                data_dic[res[r]] = stream
            else:
                data_dic[res[r]] = 0
        return data_dic,audio,my.title

    def progress_to_my_url(self,url):
        video,audio,title = self.get_stream(url)
        #video_res = video.get(res)
        return video,audio,title

    def progress_function(self,stream, chunk,bytes_remaining):
        size = stream.filesize
        percent = (100 * (size - bytes_remaining)) / size
        self.progress['value'] = percent
        self.progress.update()


class ProgressBarFrame(Download):
    def __init__(self,layer):
        progress = pE.start_progress_bar(layer)
        super().__init__(progress)


class BaseRadioButton:
    my_resolution = {}

    def __init__(self,layer,variable,command,pick):
        #self.layer = layer
        self.pick = pick
        self.radSel = variable

        values = {"144p" : "1",
                  "360p" : "2",
                  "720p" : "3",
                  "1080p": "4",
                  }
        column =[27,28,29,30]
        count = 0
        for (text,value) in values.items():
            self.button = Radiobutton(layer, text=text, value=value,variable=variable,command=command)
            self.button.grid(row=4,column=column[count])
            count += 1

    def click_one(self):
        radsel = self.radSel.get()
        if radsel == 1:
            self.my_resolution[self.pick] = '144p'
            return print(self.my_resolution)
        elif radsel == 2:
            self.my_resolution[self.pick] = '360p'
            return print(self.my_resolution)
        elif radsel == 3:
            self.my_resolution[self.pick] = '720p'
            return print(self.my_resolution)
        elif radsel == 4:
            self.my_resolution[self.pick] = '1080p'
            return print(self.my_resolution)


class FrameOneBaseRadioButton(BaseRadioButton):
    def __init__(self,layer):
        frame_1 = IntVar()
        command = self.click_one
        pick = my_gui.new_link[0]
        super().__init__(layer,frame_1,command,pick)


class FrameTwoBaseRadioButton(BaseRadioButton):
    def __init__(self, layer):
        frame_2 = IntVar()
        command = self.click_one
        self.radSel = frame_2
        pick = my_gui.new_link[1]
        super().__init__(layer, frame_2, command,pick)


class FrameThreeBaseRadioButton(BaseRadioButton):
    def __init__(self, layer):
        frame_3 = IntVar()
        command = self.click_one
        self.radSel = frame_3
        pick = my_gui.new_link[2]
        super().__init__(layer, frame_3, command,pick)


class FrameFourBaseRadioButton(BaseRadioButton):
    def __init__(self, layer):
        frame_4 = IntVar()
        command = self.click_one
        self.radSel = frame_4
        pick = my_gui.new_link[3]
        super().__init__(layer, frame_4, command,pick)


class FrameFiveBaseRadioButton(BaseRadioButton):
    def __init__(self, layer):
        frame_5 = IntVar()
        command = self.click_one
        self.radSel = frame_5
        pick = my_gui.new_link[4]
        super().__init__(layer, frame_5, command,pick)


class ProgressMe:
    def progress_bar(self,my_frame):
        return ttk.Progressbar(my_frame, orient='horizontal', length=200, mode='determinate')

    def start_progress_bar(self,by_frame):
        p = self.progress_bar(by_frame)
        p.grid(row=2, column=0)
        return p


root = Tk()
my_gui = Youtube(root)
pE = ProgressMe()
root.mainloop()

